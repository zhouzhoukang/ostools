# -*- coding: utf-8 -*-
# FileName:     custom_module.py
# time:         23/1/2 002 下午 7:37
# Author:       Zhou Hang
# Description:  I don't want to write
from PyQt5.QtCore import QPoint, Qt
from PyQt5.QtGui import QMouseEvent, QTextBlockFormat
from PyQt5.QtWidgets import QMenu, QMainWindow, QTableWidgetItem, QTableWidget, QTextEdit


class RoundMainWindow(QMainWindow):
    def __init__(self, parent=None):
        super(RoundMainWindow, self).__init__(parent)
        self._tracking = False
        self._startPos = None
        self._endPos = None
        self.padding = 5  # 设置边距
        self.round_width = 20
        self.is_max = False
        # 设置 窗口无边框和背景透明 *必须
        self.setAttribute(Qt.WA_TranslucentBackground)
        self.setWindowFlags(Qt.FramelessWindowHint)

    # def paintEvent(self, event):
    #     # 圆角
    #     pat = QPainter(self)
    #     # pat.setRenderHint(pat.Antialiasing)  # 抗锯齿
    #     pat.setBrush(QColor(49, 54, 59))  # 仅仅适合qt_material中的dark_pink.xml系列主题
    #     pat.setPen(Qt.transparent)
    #
    #     rect = self.rect()
    #     # 设置矩形距离边框的距离
    #     rect.setLeft(self.padding)
    #     rect.setTop(self.padding)
    #     rect.setWidth(rect.width() - self.padding)
    #     rect.setHeight(rect.height() - self.padding)
    #     pat.drawRoundedRect(rect, self.round_width, self.round_width)

    # 无边框情况下也能移动窗口
    def mouseMoveEvent(self, e: QMouseEvent):  # 重写移动事件
        if self._tracking:
            self._endPos = e.pos() - self._startPos
            self.move(self.pos() + self._endPos)

    def mousePressEvent(self, e: QMouseEvent):
        if e.button() == Qt.LeftButton:
            self._startPos = QPoint(e.x(), e.y())
            self._tracking = True

    def mouseReleaseEvent(self, e: QMouseEvent):
        if e.button() == Qt.LeftButton:
            self._tracking = False
            self._startPos = None
            self._endPos = None

    def contextMenuEvent(self, event):
        """右击触发菜单，有一个退出功能"""
        menu = QMenu(self)
        maximizeAction = menu.addAction("缩放")
        quitAction = menu.addAction("退出")
        action = menu.exec_(self.mapToGlobal(event.pos()))
        if action == quitAction:
            self.close()
        elif action == maximizeAction:
            if self.is_max:
                self.is_max = False
                self.showNormal()
            else:
                self.is_max = True
                self.showMaximized()


class ZTableWidgetItem(QTableWidgetItem):
    def __init__(self, text):
        super(ZTableWidgetItem, self).__init__(text)
        self.setTextAlignment(int(Qt.AlignHCenter) | int(Qt.AlignVCenter))


class ZTableWidget(QTableWidget):
    def __init__(self, parent):
        super(ZTableWidget, self).__init__(parent)
        self.setStyleSheet('font-family:Source Code Pro;font-size:20px;')
        self.horizontalHeader().setStyleSheet("QHeaderView::section{font:20px '华文中宋';}")
        self.verticalHeader().setStyleSheet("QHeaderView::section{font:20px '华文中宋';}")
        self.v_headers = []
        self.h_headers = []
        self.n_row = 0
        self.n_column = 0

    def update(self):
        self.clear()
        self.setColumnCount(self.n_column)
        self.setRowCount(self.n_row)
        self.setHorizontalHeaderLabels(self.h_headers)
        self.setVerticalHeaderLabels(self.v_headers)

    def setCR(self, n_column, n_row, h_headers, v_headers, init_value=0):
        self.n_column = n_column
        self.n_row = n_row
        self.h_headers = h_headers
        self.v_headers = v_headers
        self.update()
        # self.init_item(init_value)

    def init_item(self, init_value):
        for i in range(self.n_row):
            for j in range(self.n_column):
                item = ZTableWidgetItem(str(init_value))
                self.setItem(i, j, item)

    def get_text(self, row, col):
        try:
            text = self.item(row, col).text()
        except Exception:  # 这里我不知道这是什么异常
            text = '0'
        return text

    def get_argv(self):
        argvs = []
        for j in range(self.n_column):
            argv = []
            for i in range(self.n_row):
                try:
                    value = int(self.get_text(i, j))
                except ValueError:  # 也就是输入的不是一个数字
                    value = 0
                if value != 0:
                    argv__ = self.v_headers[i] + '=' + str(value)
                    argv.append(argv__)
            argvs.append(":".join(argv))
        argvs = ",".join(argvs)
        return argvs


class ZTextEdit(QTextEdit):
    def __init__(self, parent):
        super(ZTextEdit, self).__init__(parent)

    def set_line_height(self, height):
        doc = self.document()
        text_cursor = self.textCursor()
        it = doc.begin()
        while it != doc.end():
            tbf = it.blockFormat()
            tbf.setLineHeight(height, QTextBlockFormat.LineDistanceHeight)
            text_cursor.setPosition(it.position())
            text_cursor.setBlockFormat(tbf)
            self.setTextCursor(text_cursor)
            it = it.next()
        # blockFormat = QTextBlockFormat()
        # blockFormat.setLineHeight(height, QTextBlockFormat.LineDistanceHeight)
        # textCursor = self.textCursor()
        # textCursor.setBlockFormat(blockFormat)
        # self.setTextCursor(textCursor)


def main():
    pass


if __name__ == "__main__":
    main()
