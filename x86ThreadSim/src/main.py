# -*- coding: utf-8 -*-
# FileName:     main.py
# time:         23/1/2 002 下午 7:36
# Author:       Zhou Hang
# Description:  I don't want to write
import sys

import qdarkstyle
from PyQt5.QtWidgets import QApplication
from qt_material import apply_stylesheet

from x86ThreadSim.src.gui.x86_gui import x86_gui


def main():
    app = QApplication(sys.argv)
    extra = {
        'font_size': '15px',
        'font_family': '华文中宋'
    }
    # apply_stylesheet(app, theme='dark_amber.xml', extra=extra)
    # 打开下面的开关同时将x86_gui的继承类修改为RoundMainWindow来获得最好的效果
    # app.setStyleSheet(qdarkstyle.load_stylesheet_pyqt5())
    view = x86_gui()
    view.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
