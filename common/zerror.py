# -*- coding: utf-8 -*-
# FileName:     zerror.py
# time:         22/12/13 013 下午 12:49
# Author:       Zhou Hang
# Description:  I don't want to write

class MyCoreError(Exception):
    pass


class zerror:
    def __init__(self, info):
        print(f'error: {info}')
        raise MyCoreError(info)


# useful instead of assert
def zassert(cond, _str='错误'):
    if not cond:
        mess = f'终止::{_str}'
        print(mess)
        raise MyCoreError(mess)
    # else:
    #     print(_str)
    return


def main():
    pass


if __name__ == "__main__":
    main()
