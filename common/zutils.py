# -*- coding: utf-8 -*-
# FileName:     zutils.py
# time:         23/1/6 006 下午 9:20
# Author:       Zhou Hang
# Description:  I don't want to write

def convert(_size):
    """
    将字符串的size信息转化为int类型，单位换算为字节
    例如4KB会被转化为 4 * 1024 = 4098返回出去
    """
    length = len(_size)
    lastchar = _size[length - 1]
    if (lastchar == 'k') or (lastchar == 'K'):
        m = 1024
        nsize = int(_size[0:length - 1]) * m
    elif (lastchar == 'm') or (lastchar == 'M'):
        m = 1024 * 1024
        nsize = int(_size[0:length - 1]) * m
    elif (lastchar == 'g') or (lastchar == 'G'):
        m = 1024 * 1024 * 1024
        nsize = int(_size[0:length - 1]) * m
    else:
        nsize = int(_size)
    return nsize


def main():
    pass


if __name__ == "__main__":
    main()
