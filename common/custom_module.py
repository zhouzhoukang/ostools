# -*- coding: utf-8 -*-
# FileName:     custom_module.py
# time:         23/1/9 009 下午 1:47
# Author:       Zhou Hang
# Description:  I don't want to write
from PyQt5.QtGui import QTextBlockFormat
from PyQt5.QtWidgets import QTextEdit


class ZTextEdit(QTextEdit):
    def __init__(self, parent):
        super(ZTextEdit, self).__init__(parent)

    def set_line_height(self, height):
        doc = self.document()
        text_cursor = self.textCursor()
        it = doc.begin()
        while it != doc.end():
            tbf = it.blockFormat()
            tbf.setLineHeight(height, QTextBlockFormat.LineDistanceHeight)
            text_cursor.setPosition(it.position())
            text_cursor.setBlockFormat(tbf)
            self.setTextCursor(text_cursor)
            it = it.next()


def main():
    pass


if __name__ == "__main__":
    main()
