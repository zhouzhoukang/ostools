# -*- coding: utf-8 -*-
# FileName:     custom_module.py
# time:         23/1/7 007 上午 11:08
# Author:       Zhou Hang
# Description:  I don't want to write
from PyQt5.QtWidgets import QTableWidget, QHeaderView, QSpinBox


class ZSpinBox(QSpinBox):
    def __init__(self, mini, maxi, cur=0):
        super(ZSpinBox, self).__init__()
        self.setMinimum(mini)
        self.setMaximum(maxi)
        self.setValue(cur)


class ZTableWidget(QTableWidget):
    def __init__(self, parent):
        super(ZTableWidget, self).__init__(parent)
        self.setStyleSheet('font-family:Source Code Pro;font-size:20px;')
        self.horizontalHeader().setStyleSheet("QHeaderView::section{font:20px '华文中宋';}")
        self.verticalHeader().setStyleSheet("QHeaderView::section{font:20px '华文中宋';}")
        self.v_headers = []
        self.n_row = 0
        self.h_headers = []
        self.n_column = 0

    def update(self):
        """真不知道咋起名了"""
        self.clear()
        self.setColumnCount(self.n_column)
        self.setRowCount(self.n_row)
        self.setHorizontalHeaderLabels(self.h_headers)
        self.setVerticalHeaderLabels(self.v_headers)

    def get_row_num(self):
        return self.n_row

    def remove_last_row(self):
        self.removeRow(self.n_row - 1)
        self.n_row -= 1
        self.v_headers.pop()

    def append_row(self, prompt):
        self.insertRow(self.n_row)
        self.n_row += 1
        self.v_headers.append(f'{prompt}{self.n_row - 1}')
        self.setVerticalHeaderLabels(self.v_headers)

    def get_item_value(self, row, col):
        value = self.cellWidget(row, col).value()
        return value


class ProcessRunListTable(ZTableWidget):
    """ProcessRun模拟器中设置每个进程参数的表格"""

    def __init__(self, parent):
        super(ProcessRunListTable, self).__init__(parent)

        self.v_headers = ['进程0']
        self.n_row = 1
        self.h_headers = ['指令数', 'I/O占比']
        self.n_column = 2
        # 标题栏宽度均分
        self.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.update()

    def init_row(self, row):
        sb = ZSpinBox(1, 20, 3)
        self.setCellWidget(row, 0, sb)
        sb = ZSpinBox(0, 100)
        self.setCellWidget(row, 1, sb)

    def update(self):
        super(ProcessRunListTable, self).update()
        # 给每一列添加一个spinbox
        for i in range(self.n_row):
            self.init_row(i)

    def setR(self, process_num):
        self.n_row = process_num
        self.v_headers = [f'进程{i}' for i in range(process_num)]
        self.update()

    def append_row(self, prompt):
        super(ProcessRunListTable, self).append_row(prompt)
        self.init_row(self.n_row - 1)

    def get_processlist(self) -> str:
        processlist = []
        for i in range(self.n_row):
            process = []
            for j in range(self.n_column):
                process.append(str(self.get_item_value(i, j)))
            process[-1] = str(100 - int(process[-1]))
            processlist.append(":".join(process))
        processlist = ",".join(processlist)
        return processlist


class SchedulerListTable(ZTableWidget):
    def __init__(self, parent):
        super(SchedulerListTable, self).__init__(parent)

        self.v_headers = ['作业0', '作业1', '作业2']
        self.n_row = 3
        self.h_headers = ['长度']
        self.n_column = 1

        self.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.update()

    def init_row(self, row):
        sb = ZSpinBox(1, 99, 3)
        self.setCellWidget(row, 0, sb)

    def update(self):
        super(SchedulerListTable, self).update()
        # 给每一列添加一个spinbox
        for i in range(self.n_row):
            self.init_row(i)

    def append_row(self, prompt):
        super(SchedulerListTable, self).append_row(prompt)
        self.init_row(self.n_row - 1)

    def get_jlist(self):
        jlist = []
        for i in range(self.n_row):
            jlist.append(str(self.get_item_value(i, 0)))
        return ",".join(jlist)


class MLFQJListTable(ZTableWidget):
    def __init__(self, parent):
        super(MLFQJListTable, self).__init__(parent)
        self.v_headers = ['作业0', '作业1', '作业2']
        self.n_row = 3
        self.h_headers = ['开始时间', '运行时间', 'IO产生间隔']
        self.n_column = 3

        self.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.update()

    def init_row(self, row):
        sb = ZSpinBox(-1, 1000, -1)  # 开始时间
        self.setCellWidget(row, 0, sb)
        sb = ZSpinBox(0, 1000)  # 占用CPU的总运行时间
        self.setCellWidget(row, 1, sb)
        sb = ZSpinBox(0, 1000)  # IO频率,每隔多少ms会发出一次io请求
        self.setCellWidget(row, 2, sb)

    def update(self):
        super(MLFQJListTable, self).update()
        # 给每一列添加一个spinbox
        for i in range(self.n_row):
            self.init_row(i)

    def append_row(self, prompt):
        super(MLFQJListTable, self).append_row(prompt)
        self.init_row(self.n_row - 1)

    def get_jlist(self) -> str:
        jlist = []
        for i in range(self.n_row):
            job = []
            for j in range(self.n_column):
                start_time = self.get_item_value(i, j)
                if start_time == -1:
                    return ''
                job.append(str(start_time))
            jlist.append(",".join(job))
        jlist = ":".join(jlist)
        return jlist


class MLFQListTable(ZTableWidget):
    def __init__(self, parent):
        super(MLFQListTable, self).__init__(parent)

        self.v_headers = ['队列0', '队列1', '队列2']
        self.n_row = 3
        self.h_headers = ['长度']
        self.n_column = 1

        self.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.update()

    def init_row(self, row):
        sb = ZSpinBox(0, 99)
        self.setCellWidget(row, 0, sb)

    def update(self):
        super(MLFQListTable, self).update()
        # 给每一列添加一个spinbox
        for i in range(self.n_row):
            self.init_row(i)

    def append_row(self, prompt):
        super(MLFQListTable, self).append_row(prompt)
        self.init_row(self.n_row - 1)

    def get_list(self):
        ret = []
        for i in range(self.n_row):
            length = self.get_item_value(i, 0)
            if length == 0:  # 只要有一个为0就不启用这个功能
                return ''
            ret.append(str(length))
        return ",".join(ret)


class LotteryJListTable(ZTableWidget):
    def __init__(self, parent):
        super(LotteryJListTable, self).__init__(parent)

        self.v_headers = ['作业0', '作业1', '作业2']
        self.n_row = 3
        self.h_headers = ['运行时间', '彩票数量']
        self.n_column = 2
        # 标题栏宽度均分
        self.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.update()

    def init_row(self, row):
        sb = ZSpinBox(1, 20, 3)
        self.setCellWidget(row, 0, sb)
        sb = ZSpinBox(1, 10000, 100)  # 彩票数量不能为0
        self.setCellWidget(row, 1, sb)

    def update(self):
        super(LotteryJListTable, self).update()
        # 给每一列添加一个spinbox
        for i in range(self.n_row):
            self.init_row(i)

    def append_row(self, prompt):
        super(LotteryJListTable, self).append_row(prompt)
        self.init_row(self.n_row - 1)

    def get_jlist(self) -> str:
        processlist = []
        for i in range(self.n_row):
            process = []
            for j in range(self.n_column):
                process.append(str(self.get_item_value(i, j)))
            processlist.append(":".join(process))
        processlist = ",".join(processlist)
        return processlist


class SegmentationTable(ZTableWidget):
    def __init__(self, parent):
        super(SegmentationTable, self).__init__(parent)
        self.v_headers = ['第0']
        self.n_row = 1
        self.h_headers = ['页']
        self.n_column = 1
        self.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.update()

    def init_row(self, row):
        sb = ZSpinBox(-1, 100, -1)
        self.setCellWidget(row, 0, sb)

    def update(self):
        super(SegmentationTable, self).update()
        # 给每一列添加一个spinbox
        for i in range(self.n_row):
            self.init_row(i)

    def append_row(self, prompt):
        super(SegmentationTable, self).append_row(prompt)
        self.init_row(self.n_row - 1)

    def get_addresses(self):
        ret = []
        for i in range(self.n_row):
            length = self.get_item_value(i, 0)
            ret.append(str(length))
        return ",".join(ret)


class MallocTable(ZTableWidget):
    def __init__(self, parent):
        super(MallocTable, self).__init__(parent)
        self.v_headers = [f'请求{i}' for i in range(10)]
        self.n_row = 10
        self.h_headers = ['请求信息']
        self.n_column = 1
        self.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.update()

    def init_row(self, row):
        """
        0表示使用随机请求
        -1表示free第一块
        10表示分配一个10B的内存块
        """
        sb = ZSpinBox(-100, 100000)
        self.setCellWidget(row, 0, sb)

    def update(self):
        super(MallocTable, self).update()
        # 给每一列添加一个spinbox
        for i in range(self.n_row):
            self.init_row(i)

    def append_row(self, prompt):
        super(MallocTable, self).append_row(prompt)
        self.init_row(self.n_row - 1)

    def get_allocList(self):
        ret = []
        for i in range(self.n_row):
            length = self.get_item_value(i, 0)
            if length == 0:
                return ''
            ret.append(str(length))
        return ",".join(ret)


class LinearTable(ZTableWidget):
    def __init__(self, parent):
        super(LinearTable, self).__init__(parent)
        self.v_headers = [f'页{i}' for i in range(10)]
        self.n_row = 1
        self.h_headers = ['页号']
        self.n_column = 1
        self.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.update()

    def init_row(self, row):
        sb = ZSpinBox(0, 1000)
        self.setCellWidget(row, 0, sb)

    def update(self):
        super(LinearTable, self).update()
        for i in range(self.n_row):
            self.init_row(i)

    def append_row(self, prompt):
        super(LinearTable, self).append_row(prompt)
        self.init_row(self.n_row - 1)

    def get_addresses(self):
        ret = []
        for i in range(self.n_row):
            length = self.get_item_value(i, 0)
            if length == 0:
                return '-1'
            ret.append(str(length))
        return ",".join(ret)



def main():
    pass


if __name__ == "__main__":
    main()
