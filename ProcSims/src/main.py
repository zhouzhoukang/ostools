# -*- coding: utf-8 -*-
# FileName:     main.py
# time:         23/1/6 006 下午 4:42
# Author:       Zhou Hang
# Description:  I don't want to write
import sys
from PyQt5.QtWidgets import QApplication
from ProcSims.src.gui.zsimulators import ZSimulators


def main():
    app = QApplication(sys.argv)
    view = ZSimulators()
    view.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
