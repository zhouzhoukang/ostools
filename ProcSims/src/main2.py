# -*- coding: utf-8 -*-
# FileName:     main2.py
# time:         23/1/12 012 下午 8:45
# Author:       Zhou Hang
# Description:  I don't want to write

import sys
from PyQt5.QtWidgets import QApplication
from ProcSims.src.gui.zsimulators2 import ZSimulators


def main():
    app = QApplication(sys.argv)
    view = ZSimulators()
    view.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
