# -*- coding: utf-8 -*-
# FileName:     main.py
# Time:         23/8/4 004 上午 11:47
# Author:       Zhou Hang
# Description:  RAID 模拟器 GUI

import sys

import qdarkstyle
from PyQt5.QtWidgets import QApplication

from RaidSim.src.gui.raid_simulator_gui import Raid_Simulator_gui


def main():
    app = QApplication(sys.argv)
    app.setStyleSheet(qdarkstyle.load_stylesheet_pyqt5())
    view = Raid_Simulator_gui()
    view.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
