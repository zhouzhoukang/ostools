# -*- coding: utf-8 -*-
# FileName:     z2to3.py
# time:         22/12/4 004 下午 3:28
# Author:       Zhou Hang
# Description:  自己编写python2转python3工具脚本，目前只能实现print语法的转变

from pathlib import Path


def main():
    py2_file = Path('./raid_bak.py')
    py3_file = Path('../core/raid.py')
    py2_temp = []  # 把文件读取到内存中处理
    py3_temp = []  # 存放处理好的python3脚本，最终将这些都放在py2文件中
    with py2_file.open(encoding='utf-8') as f:
        for line in f:
            py2_temp.append(line)

    # 开始处理文
    idx = 0
    while idx < len(py2_temp):
        line = py2_temp[idx]
        if line.strip().startswith('print'):
            next_line = py2_temp[idx + 1]
            new_line = f'{line.rstrip()}({next_line.strip()})\n'
            py3_temp.append(new_line)
            idx += 2
        else:
            py3_temp.append(line)
            idx += 1

    for line in py3_temp:
        print(line, end='')

    py3_file.write_text(''.join(py3_temp), encoding='utf-8')


if __name__ == "__main__":
    main()
