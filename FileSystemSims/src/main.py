# -*- coding: utf-8 -*-
# FileName:     main.py
# time:         23/2/1 001 下午 8:54
# Author:       Zhou Hang
# Description:  持久化存储 文件系统模拟器

import sys
from PyQt5.QtWidgets import QApplication
from FileSystemSims.src.gui.persistence_gui import Persistence


def main():
    app = QApplication(sys.argv)
    view = Persistence()
    view.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
