# File System Simulators

> 多个文件系统相关模拟器整合, 包括`简单文件系统(VSFS)`, `快速文件系统(FFS)`, `文件系统检查(FSCK)`, `日志文件系统(LFS)`, `固态存储设备(SSD)`, `完整性检查(Checksum)`, `Andrew文件系统 (AFS)`

## 简单文件系统VSFS

- 该模拟器将硬盘简化为如下四个数据结构

![VSFS](README.assets/VSFS.png)

- 下面的例子表示了当前文件系统的状态:

```bash
inode bitmap 11110000
inodes       [d a:0 r:6] [f a:1 r:1] [f a:-1 r:1] [d a:2 r:2] [] ...
data bitmap  11100000
data 		 [(.,0) (..,0) (y,1) (z,2) (f,3)] [u] [(.,3) (..,0)] [] ...
```

- inode bitmap: 总共有八个 inode, 前四个当前已经被分配.
- inodes: 每个方括号代表一个 inode.
- data bitmap: 总共有八个 data block, 前四个被分配.
- data blocks: 每个方括号代表一个 data block.

## 快速文件系统 FFS

- 使用前需要将 I/O 操作写在 ../input/in 文件中.
- 该文件系统结构如下

![FFS](README.assets/FFS.png)

## 文件系统检查 FSCK

- 一些老式文件系统采用 FSCK 的方法克服崩溃一致性问题. 文件系统模型基于 VSFS.

## 日志文件系统 LFS

## 固态存储设备 SSD

## Checksum

## Andrew 文件系统 AFS

